
<?php 
    include_once('header.php');
    if(isset($_SESSION['codigo']) && isset($_SESSION['nome'])){
        header('Location: index.php');
    }
?>
<!-- Signup-->
<section class="signup-section" id="signup" style="background-image: url('assets/img/dark-polygonal-background_1409-878.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <i class="fas fa-user fa-2x mb-2 text-white"></i>
                <h2 class="text-white mb-5">Criar conta</h2>
            </div>
        </div>
    </div>
</section> 
<section class="login-section" id="login">
    <div class="container">
        <form class="mt-5 mb-5" id="formCadastro" method="POST" action="cadastrar.php">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="inputNome">Nome:</label>
                    <input type="text" class="form-control form-control-lg" id="inputNome" name="inputNome" placeholder="Seu nome" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputSobrenome">Sobrenome:</label>
                    <input type="text" class="form-control form-control-lg" id="inputSobrenome" name="inputSobrenome" placeholder="Seu sobrenome" required>
                </div>
            </div>    
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="inputEmail">E-mail:</label>
                    <input type="email" class="form-control form-control-lg" id="inputEmail" name="inputEmail" placeholder="Seu e-mail" required>
                </div>
            </div> 
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="inputCPF">CPF:</label>
                    <input type="text" class="form-control form-control-lg" id="inputCPF" name="inputCPF" placeholder="Seu CPF" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputTelefone">Telefone:</label>
                    <input type="tel" class="form-control form-control-lg" id="inputTelefone" name="inputTelefone" placeholder="Seu telefone" required>
                </div>
            </div>     
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="inputSenha">Senha:</label>
                    <input type="password" class="form-control form-control-lg" id="inputSenha" name="inputSenha" placeholder="Sua senha" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputConfirma">Confirmação de senha:</label>
                    <input type="password" class="form-control form-control-lg" id="inputConfirma" name="inputConfirma" placeholder="Repita sua senha" required>
                </div>
            </div>     
            <div class="form-row mt-3">
                <div class="col-xs-12 col-md-3 offset-md-6">
                    <button class="btn btn-light fw" id="btnLimpar">Limpar</button>
                </div>
                <div class="col-xs-12 col-md-3">
                    <input type="submit" class="btn btn-primary fw" name="enviar" id="enviar" value="Enviar">
                </div>
                

            </div>
        </form>
    </div>
    <?php 
        if(isset($_POST["enviar"])) {
            $nome = $_POST['inputNome'];
            $sobrenome = $_POST['inputSobrenome'];
            $email = $_POST['inputEmail'];
            $cpf = $_POST['inputCPF'];
            $telefone = $_POST['inputTelefone'];
            $senha = $_POST['inputSenha'];
            $confirma = $_POST['inputConfirma'];

            if($senha == $confirma){
                $senha = md5($senha);
                include_once('conn.php');
                $sql = "INSERT INTO usuarios (nome, sobrenome, email, cpf, telefone, senha) VALUES ('$nome', '$sobrenome', '$email', '$cpf', '$telefone', '$senha')";
                $err = mysqli_query($conn, $sql);
                echo "<h4>Registro inserido com sucesso</h4>";
                $conn -> close();
            } else {
                $_SESSION['error'] = "As senhas não batem!";
            }

            
            
        }
    ?>
</section>           
<?php include_once('footer.php') ?>