<?php 
    include_once('header.php') ;
    if(!isset($_SESSION)) session_start();
    if(!isset($_SESSION['codigo']) && !isset($_SESSION['nome'])){
        $_SESSION['error'] = "Faça login para começar as compras!";
        header('Location: login.php');
    }
?>
<!-- Signup-->
<section class="signup-section" id="signup" style="background-image: url('assets/img/dark-polygonal-background_1409-878.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <i class="fas fa-shopping-cart fa-2x text-white"></i>
                <h2 class="text-white mb-5">Carrinho</h2>
            </div>
        </div>
    </div>
</section> 

<section id="cart-section mt-5 mb-5">
    <div class="container">
        <table class="table table-bordered table-dark mt-5 mb-5 col-xs-12">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Item</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Qtd</th>
                    <th scope="col">Remover</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $subtotal = 00.00;
                    include_once('conn.php');
                    $id_usuario = $_SESSION['codigo'];
                    $result_produtos = "SELECT * FROM carrinho WHERE cd_usuario = $id_usuario ORDER BY cd_produto ASC";
                    $resultado_produtos = mysqli_query($conn, $result_produtos);
                    while($rows_produtos = mysqli_fetch_assoc($resultado_produtos)){
                        $cd_prod = $rows_produtos['cd_produto'];
                        $result = "SELECT * FROM produtos WHERE codigo = $cd_prod";
                        $resultado = mysqli_query($conn, $result);
                        $row = mysqli_fetch_assoc($resultado);
                        $preco = str_replace(',', '.', $row['preco']);
                        $total = $preco * $rows_produtos['quantidade'];
                        $total = number_format($total, 2, '.', '');
                        $subtotal += $total;
                        $total = str_replace('.', ',', $total);
                        
                ?>
                    <tr>
                        <th scope="row"><?php echo $rows_produtos['cd_produto']; ?></th>
                        <td><?php echo $row['nome']; ?></td>
                        <td>R$ <?php echo $row['preco']; ?></td>
                        <td><?php echo $rows_produtos['quantidade']; ?></td>
                        <td><a href="remover.php?id=<?php echo $rows_produtos['codigo']; ?>"><center> Remover</center></a></td>
                        <td>R$ <?php echo $total ?></td>
                    </tr>
                <?php
                    }
                ?>
                <tr>
                    <th scope="row">-</th>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <th scope="row">-</th>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <th scope="row">-</th>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <?php
                    if(isset($subtotal)){ 
                        $subtotal = number_format($subtotal, 2, '.', '');
                        $subtotal = str_replace('.', ',', $subtotal);
                    }
                ?>
                <tr>
                    <th scope="row">-</th>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>R$ <?php echo $subtotal; ?></td>
                </tr>
                
            </tbody>

            
        </table>
        
        <button class="btn btn-success mb-5"><i class="fas fa-shopping-cart text-white"></i> Finalizar compra</button>
        <a href="esvaziar.php" class="btn btn-light mb-5">Limpar cesta</a>
    </div>
</section>

<?php include_once('footer.php') ?>