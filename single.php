<?php 
    include_once('header.php');
    session_start();
    include_once('conn.php');
    $id = $_GET['id_prod'];
	$result_usuario = "SELECT * FROM produtos WHERE codigo = $id";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$resultado = mysqli_fetch_assoc($resultado_usuario);
?>
<!-- Signup-->
<section class="signup-section" id="signup" style="background-image: url('assets/img/dark-polygonal-background_1409-878.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <img width="350" src="assets/img/<?php echo $resultado["imagem"]; ?>" alt="<?php echo $resultado['nome']; ?>">
                <h2 class="text-white mt-3"><?php echo $resultado['nome']; ?></h2>
                <h4 class="text-success mb-5">R$<?php echo $resultado['preco']; ?></h4>
                <p class="text-light"><?php echo $resultado['descricao']; ?></p>
            </div>
        </div>
    </div>
</section> 
<?php include_once('footer.php') ?>