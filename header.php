<?php 
    session_start(); 
    if(isset($_SESSION['codigo']) && isset($_SESSION['nome'])){
        $codigo = $_SESSION['codigo'];
        $nome = $_SESSION['nome'];
    }
    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Claybom</title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="index.php" title="Home">Claybom</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a title="Produtos" class="nav-link js-scroll-trigger" href="products.php">Produtos</a></li>
                        <li class="nav-item"><a title="Pesquisar produtos" class="nav-link js-scroll-trigger" href="pesquisar.php"><i class="fa fa-search"></i></a></li>
                        <li class="nav-item"><a title="Carrinho" class="nav-link js-scroll-trigger" href="carrinho.php"><i class="fa fa-shopping-cart"></i></a></li>
                        <?php if(!isset($_SESSION['codigo'])){?> 
                            <li class="nav-item"><a title="Entrar" class="nav-link js-scroll-trigger" href="login.php"><i class="fa fa-user"></i></a></li>
                        <?php }else{ ?>
                            <li class="nav-item"><a title="Sair" class="nav-link js-scroll-trigger" href="logout.php"><i class="fas fa-sign-out-alt"></i></a></li>
                        <?php } ?>
                        
                        
                    </ul>
                </div>
            </div>
        </nav>