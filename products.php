<?php include_once('header.php') ?>
<section class="signup-section" id="signup" style="background-image: url('assets/img/dark-polygonal-background_1409-878.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <i class="fas fa-shopping-bag fa-2x mb-2 text-white"></i>
                <h2 class="text-white mb-5">Produtos</h2>
            </div>
        </div>
    </div>
</section> 
<section class="products-section" id="products">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <i class="fas fa-shopping-bag mt-5 fa-4x mb-2 text-black"></i>
                <h2 class="text-black mb-5">Estes abaixo são alguns de nossos produtos:</h2>
            </div>
        </div>
        <div class="row mb-5">
            <?php 
                include_once('conn.php');
                $result_produtos = "SELECT * FROM produtos";
                $resultado_produtos = mysqli_query($conn, $result_produtos);
                while($rows_produtos = mysqli_fetch_assoc($resultado_produtos)){ ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/<?php echo $rows_produtos['imagem']; ?>" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $rows_produtos['nome']; ?></h5>
                                <h4 class="text-success">R$<?php echo $rows_produtos['preco']; ?></h4>
                                <p class="card-text"><?php echo mb_strimwidth($rows_produtos['descricao'], 0, 180, "...");?></p>
                                <a class="btn btn-primary js-scroll-trigger" href="adicionar.php?id_prod=<?php echo $rows_produtos['codigo']; ?>">COMPRAR</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            

            
        </div>
            </div>
    </div>
</section>    
<?php include_once('footer.php') ?>