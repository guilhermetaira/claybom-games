
    (function ($) {
    "use strict"; 


    setTimeout(function(){ $('.toast2').addClass('on'); }, 500);
    setTimeout(function(){ $('.toast2').removeClass('on'); }, 4000);

    
    $('.close').click(function() {
        $('.toast2').removeClass('on');
    });

    $("#btnLimpar").click(function () {
        $('#formCadastro').trigger("reset");
    });

    $("#btnModal").click(function () {
        $('#myModal').modal(options)
    });

    $("#btnProduct").click(function () {
        $('#productDetails').modal(options)
    });
    
    

    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 70,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    $("body").scrollspy({
        target: "#mainNav",
        offset: 100,
    });

    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    navbarCollapse();
    $(window).scroll(navbarCollapse);
})(jQuery); 