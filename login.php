<?php 
    include_once('header.php');
    if(!isset($_SESSION)) session_start();
    
    if(isset($_POST['inputEmail']) && isset($_POST['inputSenha'])){
        $usuario = $_POST['inputEmail'];
        $senha = md5($_POST['inputSenha']);
        include_once('conn.php');
		$result_usuario = "SELECT * FROM usuarios WHERE email = '$usuario' && senha = '$senha' LIMIT 1";
		$resultado_usuario = mysqli_query($conn, $result_usuario);
		$resultado = mysqli_fetch_assoc($resultado_usuario);
		
		if(isset($resultado)){
			$_SESSION['codigo'] = $resultado['codigo'];
            $_SESSION['nome'] = $resultado['nome'];
            $_SESSION['email'] = $resultado['email'];
            $_SESSION['mudar'] = $resultado['mudar'];
            echo $_SESSION['email'];
		}else{	
			$_SESSION['error'] = "E-mail e/ou senha inválido(s)!";
        }
    }
    
    if(isset($_SESSION['codigo']) && isset($_SESSION['nome'])){
        if($_SESSION['mudar'] == 0):
            header('Location: index.php');
        else:
            header('Location: mudar.php');
        endif;
        
    }

    
?>
<!-- Signup-->
<section class="signup-section" id="signup" style="background-image: url('assets/img/dark-polygonal-background_1409-878.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <i class="fas fa-user fa-2x mb-2 text-white"></i>
                <h2 class="text-white mb-5">Login</h2>
            </div>
        </div>
    </div>
</section> 
<section class="login-section" id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2 class="text-black text-center mb-3 mt-5 col-xs-12">Já tenho cadastro.</h2>
                <form class="col-xs-12 mb-5" action="login.php" method="POST">
                    <label for="inputEmail">E-mail:</label>
                    <input type="email" name="inputEmail" id="inputEmail" class="form-control form-control-lg" required>
                    <!-- <?php echo isset($_SESSION['email']) ? $_SESSION['email'] : ""; ?> -->
                    <label for="inputSenha" class="mt-4">Senha:</label>
                    <input type="password" name="inputSenha" id="inputSenha" class="form-control form-control-lg mb-1" required>
                    <small><a href="esqueci.php" class="mb-4" >Esqueceu a senha?</a></small>
                    <input type="submit" value="Login" class="btn btn-primary col-xs-12 fw">
                </form>
            </div>
            <div class="col-xs-12 col-sm-5 offset-sm-1 flex-center">
                <div class="">
                    <h2 class="text-black text-center mb-3 mt-5 col-xs-12">Ainda não tenho cadastro</h2>
                    <a href="cadastrar.php" class="btn btn-primary fw mt-3">
                        Clique aqui
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>   
<?php include_once('footer.php') ?>