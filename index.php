<?php include_once('header.php') ?>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container d-flex h-100 align-items-center">
                <div class="mx-auto text-center">
                    <h1 class="mx-auto my-0 text-uppercase">CLAYBOM</h1>
                    <h2 class="text-white-50 mx-auto mt-2 mb-5">A sua loja de jogos na net.</h2>
                    <a class="btn btn-primary js-scroll-trigger" href="#products">VER PRODUTOS</a>
                </div>
            </div>
        </header>
        <!-- About-->
        <section class="about-section text-center" id="products">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <h2 class="text-white mb-4">Veja os nossos produtos mais vendidos:</h2>
                        <p class="text-white-50">
                            Não pense duas vezes!<br/> Esses são os jogos bombados do momento, vem jogar com a gente!
                        </p>
                    </div>
                </div>
                <!--<img class="img-fluid" src="assets/img/ipad.png" alt="" />-->
                
                <div class="row mb-5">
                    <?php 
                        include_once('conn.php');
                        $result_produtos = "SELECT * FROM produtos WHERE destaque = 1 LIMIT 4";
                        $resultado_produtos = mysqli_query($conn, $result_produtos);
                        while($rows_produtos = mysqli_fetch_assoc($resultado_produtos)){ ?>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="card">
                                    <img class="card-img-top" src="assets/img/<?php echo $rows_produtos['imagem']; ?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title"><?php echo $rows_produtos['nome']; ?></h5>
                                        <h4 class="text-success">R$<?php echo $rows_produtos['preco']; ?></h4>
                                        <p class="card-text"><?php echo mb_strimwidth($rows_produtos['descricao'], 0, 180, "...");?></p>
                                        <a class="btn btn-secondary js-scroll-trigger mb-3" href="single.php?id_prod=<?php echo $rows_produtos['codigo']; ?>">DETALHES</a>
                                        <a class="btn btn-primary js-scroll-trigger" href="adicionar.php?id_prod=<?php echo $rows_produtos['codigo']; ?>">COMPRAR</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                
            </div>
        </section>
        <!-- Projects-->
        <section class="projects-section bg-light" id="projects">
            <div class="container">
                <!-- Featured Project Row-->
                <div class="row align-items-center no-gutters mb-4 mb-lg-5">
                    <div class="col-xl-8 col-lg-7"><img class="img-fluid mb-3 mb-lg-0" src="assets/img/zelda.jpg" alt="" /></div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="featured-text text-center text-lg-left">
                            <h4>Zelda</h4>
                            <p class="text-black-50 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
                <!-- Project One Row-->
                <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                    <div class="col-lg-6"><img class="img-fluid" src="assets/img/mario.jpg" alt="" /></div>
                    <div class="col-lg-6">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-left">
                                    <h4 class="text-white">Super Mario Flashback</h4>
                                    <p class="mb-0 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                    <hr class="d-none d-lg-block mb-0 ml-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Project Two Row-->
                <div class="row justify-content-center no-gutters">
                    <div class="col-lg-6"><img class="img-fluid" src="assets/img/sonic.jpg" alt="" /></div>
                    <div class="col-lg-6 order-lg-first">
                        <div class="bg-black text-center h-100 project">
                            <div class="d-flex h-100">
                                <div class="project-text w-100 my-auto text-center text-lg-right">
                                    <h4 class="text-white">Sonic: The Hedgehog</h4>
                                    <p class="mb-0 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                    <hr class="d-none d-lg-block mb-0 mr-0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal bd-example-modal-lg" id="productDetails" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Minecraft</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-5 mt-5">
                        <div class="col-xs-12 col-md-4">
                            <img class="fw" src="assets/img/minecraft.png" alt="Card image cap">
                        </div>
                        <div class="col-xs-12 col-md-8">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <div class="row">
                                <div class="col-xs-12 col-md-3 offset-md-8">
                                    <a href="" class="btn btn-primary">COMPRAR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        
<?php include_once('footer.php') ?>